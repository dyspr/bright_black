var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var numOfBalls = 16
var balls = []

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  for (var i = 0; i < numOfBalls; i++) {
    balls.push(new Balls(i))
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  noStroke()
  fill(255)
  arc(windowWidth * 0.5, windowHeight * 0.5, boardSize * 0.875 + 2, boardSize * 0.875 + 2, 0 + frameCount * 0.01, Math.PI + frameCount * 0.01)
  noStroke()
  fill(0)
  arc(windowWidth * 0.5, windowHeight * 0.5, boardSize * 0.875 + 2, boardSize * 0.875 + 2, Math.PI + frameCount * 0.01, Math.PI * 2 + frameCount * 0.01)

  push()
  translate(boardSize * 0.875 * 0.25 * cos(frameCount * 0.01), boardSize * 0.875 * 0.25 * sin(frameCount * 0.01))
  fill(255)
  stroke(255)
  strokeWeight(2)
  arc(windowWidth * 0.5, windowHeight * 0.5, boardSize * 0.875 * 0.5, boardSize * 0.875 * 0.5, Math.PI + frameCount * 0.01, Math.PI * 2 + frameCount * 0.01, CHORD)
  pop()

  push()
  translate(-boardSize * 0.875 * 0.25 * cos(frameCount * 0.01), -boardSize * 0.875 * 0.25 * sin(frameCount * 0.01))
  fill(0)
  stroke(0)
  strokeWeight(2)
  arc(windowWidth * 0.5, windowHeight * 0.5, boardSize * 0.875 * 0.5, boardSize * 0.875 * 0.5, 0 + frameCount * 0.01, Math.PI + frameCount * 0.01, CHORD)
  pop()

  for (var i = 0; i < numOfBalls; i++) {
    balls[i].display()
    balls[i].move()
  }
}

class Balls {
  constructor(x) {
    this.limit = 0.8
    this.dist = Math.random() * 2 * this.limit - this.limit
    this.dir = Math.random() * 2
    this.x = sin(this.dir * Math.PI * 2) * this.dist
    this.y = cos(this.dir * Math.PI * 2) * this.dist
    this.xspeed = 0.01 + 0.02 * Math.random()
    this.yspeed = 0.01 + 0.02 * Math.random()
    this.size = 0.1 + 0.1 * Math.random()
    this.color = x % 2
  }

  display() {
    fill(255 * this.color)
    noStroke()
    ellipse(windowWidth * 0.5 + 0.4 * boardSize * this.x, windowHeight * 0.5 + 0.4 * boardSize * this.y, boardSize * this.size)
  }

  move() {
    this.x += this.xspeed * sin(this.dir * Math.PI * 2)
    this.y += this.yspeed * cos(this.dir * Math.PI * 2)
    if (dist(this.x, this.y, 0, 0) > 1 - this.size) {
      this.xspeed *= -1
      this.yspeed *= -1
      this.dir += 1
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
